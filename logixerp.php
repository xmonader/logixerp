<?php
require_once 'Requests/library/Requests.php';

Requests::register_autoloader();

class LogixERP{
    function __construct($api_endpoint, $api_key)
    {
        $this->endpoints = array();
        $this->api_key = $api_key;
        $this->endpoints["CreateWaybill"] = "CreateWaybill?secureKey={$this->api_key}";
        // $default_endpoint = "CreateWaybill?secureKey={$this.api_key}"
        $this->api_endpoint = $api_endpoint ;

    }
    function createWaybill($waybillData=null){
        
        $samplebody = <<<BODY
        "wayBillRequestData":{  
            "consigneeGeoLocation":"20.63,54.89",
            "FromOU":"",
            "DeliveryDate":"2018-12-24",
            "WaybillNumber":"",
            "CustomerCode":"KOOVS1",
            "ConsigneeCode":"",
            "ConsigneeName":"xyzsc",
            "ConsigneePhone":"7888",
            "ConsigneeAddress":"test address",
            "ConsigneeCountry":"IN",
            "ConsigneeState":"HR",
            "ConsigneeCity":"Gurgaon",
            "ConsigneePincode":"110020",
            "ConsigneeWhat3Words":"word.exact.replace",
            "StartLocation":"Agra",
            "EndLocation":"Manesar",
            "ClientCode":"KOOVS1",
            "NumberOfPackages":"1",
            "ActualWeight":"15",
            "ChargedWeight":"",
            "CargoValue":"",
            "ReferenceNumber":"",
            "InvoiceNumber":"",
            "PaymentMode":"TBB",
            "ServiceCode":"PARTLOAD",
            "WeightUnitType":"KILOGRAM",
            "Description":"This docket is for testing purpose only",
            "COD":"",
            "CODPaymentMode":"",
            "PackageDetails":{  
                "packageJsonString":[  
                    {  
                    "barCode":"A101",
                    "packageCount":1,
                    "length":10,
                    "width":10,
                    "heig inht":10,
                    "weight":0,
                    "chargedWeight":2000
                    },
                    {  
                    "barCode":"A102",
                    "packageCount":10,
                    "length":10,
                    "width":10,
                    "height":10,
                    "weight":1,
                    "chargedWeight":"5000.00"
                    }
                ]
            }
        }
BODY;
        $json_body = "";
        if($waybillData != null){
            $json_body = $waybillData;
        }else{
            $json_body = json_decode($samplebody);
        }

        $res = Requests::post("{$this->api_endpoint}/{$this->endpoints['CreateWaybill']}", $json_body);
        $resbody = $res->body;
        $json_resp = json_decode($resbody);
        if ($json_resp->messageType =="Success"){
            return $json_resp;

        }else{
            throw new Exception($json_resp->message);
        }
        // ON SUCCESS

        // {  
        //    "labelURL":"https://LABEL_DOWNLOAD_URLpdf",
        //    "message":"Waybill Created Successfully",
        //    "messageType":"Success",
        //    "status":"Data Received",
        //    "waybillNumber":"DELHI11639"
        // }

        

        // ON ERROR

        // {"message":"ERROR_MESSAGE","messageType":"Error"}
    }
}   


// $response = Requests::get('https://github.com/timeline.json');
// var_dump($response->body);

$erp = new LogixERP("https://webservice.logixerp.com/webservice/v2/", "0000000000000000000");
$erp->createWaybill();